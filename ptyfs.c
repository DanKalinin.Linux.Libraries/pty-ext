//
// Created by root on 05.12.2021.
//

#include "ptyfs.h"

gboolean pty_fs_mount_sync(gchar *source, gchar *directory, GError **error) {
    g_autofree gchar *usr = g_build_filename(directory, "usr", NULL);
    g_autofree gchar *home = g_build_filename(directory, "home", NULL);
    if (g_file_test(usr, G_FILE_TEST_EXISTS) && g_file_test(home, G_FILE_TEST_EXISTS)) return TRUE;

    g_autoptr(AREArchive) archive = archive_read_new();
    if (are_archive_read_support_filter_gzip(archive, error) < ARCHIVE_OK) return FALSE;
    if (are_archive_read_support_format_tar(archive, error) < ARCHIVE_OK) return FALSE;
    if (are_archive_read_open_filename(archive, source, 10240, error) < ARCHIVE_OK) return FALSE;
    AREEntry *entry = NULL;

    while (are_archive_read_next_header(archive, &entry, error) == ARCHIVE_OK) {
        gchar *pathname = (gchar *)archive_entry_pathname(entry);
        g_autofree gchar *filename = g_build_filename(directory, pathname, NULL);
        archive_entry_set_pathname(entry, filename);
        if (are_archive_read_extract(archive, entry, (ARCHIVE_EXTRACT_PERM | ARCHIVE_EXTRACT_UNLINK), error) < ARCHIVE_OK) return FALSE;
    }

    return TRUE;
}

gboolean pty_fs_mount_v1_sync(gpointer source_data, gsize source_n, gchar *directory, GError **error) {
    g_autofree gchar *usr = g_build_filename(directory, "usr", NULL);
    g_autofree gchar *home = g_build_filename(directory, "home", NULL);
    if (g_file_test(usr, G_FILE_TEST_EXISTS) && g_file_test(home, G_FILE_TEST_EXISTS)) return TRUE;

    g_autoptr(AREArchive) archive = archive_read_new();
    if (are_archive_read_support_filter_gzip(archive, error) < ARCHIVE_OK) return FALSE;
    if (are_archive_read_support_format_tar(archive, error) < ARCHIVE_OK) return FALSE;
    if (are_archive_read_open_memory(archive, source_data, source_n, error) < ARCHIVE_OK) return FALSE;
    AREEntry *entry = NULL;

    while (are_archive_read_next_header(archive, &entry, error) == ARCHIVE_OK) {
        gchar *pathname = (gchar *)archive_entry_pathname(entry);
        g_autofree gchar *filename = g_build_filename(directory, pathname, NULL);
        archive_entry_set_pathname(entry, filename);
        if (are_archive_read_extract(archive, entry, (ARCHIVE_EXTRACT_PERM | ARCHIVE_EXTRACT_UNLINK), error) < ARCHIVE_OK) return FALSE;
    }

    return TRUE;
}

//
// Created by root on 03.12.2021.
//

#ifndef LIBRARY_PTY_EXT_PTYINIT_H
#define LIBRARY_PTY_EXT_PTYINIT_H

#include "ptymain.h"

G_BEGIN_DECLS

void pty_init(void);

G_END_DECLS

#endif //LIBRARY_PTY_EXT_PTYINIT_H

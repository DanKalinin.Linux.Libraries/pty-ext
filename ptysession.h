//
// Created by root on 05.12.2021.
//

#ifndef LIBRARY_PTY_EXT_PTYSESSION_H
#define LIBRARY_PTY_EXT_PTYSESSION_H

#include "ptymain.h"

G_BEGIN_DECLS

#define PTY_TYPE_SESSION pty_session_get_type()

GE_DECLARE_DERIVABLE_TYPE(PTYSession, pty_session, PTY, SESSION, GEObject)

struct _PTYSessionClass {
    GEObjectClass super;
};

struct _PTYSession {
    GEObject super;

    GPid pid;
    gint in;
    gint out;
    gint err;
};

GE_STRUCTURE_FIELD_INT(pty_session, pid, PTYSession, GPid, NULL, g_spawn_close_pid, NULL)
GE_STRUCTURE_FIELD_INT(pty_session, in, PTYSession, gint, NULL, close, NULL)
GE_STRUCTURE_FIELD_INT(pty_session, out, PTYSession, gint, NULL, close, NULL)
GE_STRUCTURE_FIELD_INT(pty_session, err, PTYSession, gint, NULL, close, NULL)

PTYSession *pty_session_new(gchar *cwd, GStrv argv, GStrv env, GError **error);

gboolean pty_session_kill(PTYSession *self, GError **error);

G_END_DECLS

#endif //LIBRARY_PTY_EXT_PTYSESSION_H

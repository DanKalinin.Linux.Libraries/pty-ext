//
// Created by root on 05.12.2021.
//

#include "ptysession.h"

void pty_session_finalize(GObject *self);

G_DEFINE_TYPE(PTYSession, pty_session, GE_TYPE_OBJECT)

static void pty_session_class_init(PTYSessionClass *class) {
    G_OBJECT_CLASS(class)->finalize = pty_session_finalize;
}

static void pty_session_init(PTYSession *self) {
    self->pid = -1;
    self->in = -1;
    self->out = -1;
    self->err = -1;
}

void pty_session_finalize(GObject *self) {
    pty_session_set_pid(PTY_SESSION(self), -1);
    pty_session_set_in(PTY_SESSION(self), -1);
    pty_session_set_out(PTY_SESSION(self), -1);
    pty_session_set_err(PTY_SESSION(self), -1);

    G_OBJECT_CLASS(pty_session_parent_class)->finalize(self);
}

PTYSession *pty_session_new(gchar *cwd, GStrv argv, GStrv env, GError **error) {
    GPid pid = -1;
    gint in = -1;
    gint out = -1;
    gint err = -1;
    if (!g_spawn_async_with_pipes(cwd, argv, env, G_SPAWN_CLOEXEC_PIPES, NULL, NULL, &pid, &in, &out, &err, error)) return NULL;

    PTYSession *self = g_object_new(PTY_TYPE_SESSION, NULL);
    self->pid = pid;
    self->in = in;
    self->out = out;
    self->err = err;
    return self;
}

gboolean pty_session_kill(PTYSession *self, GError **error) {
    if (ge_kill(self->pid, SIGKILL, error) == -1) return FALSE;
    return TRUE;
}

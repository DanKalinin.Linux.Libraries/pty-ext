//
// Created by root on 03.12.2021.
//

#ifndef LIBRARY_PTY_EXT_PTY_EXT_H
#define LIBRARY_PTY_EXT_PTY_EXT_H

#include <pty-ext/ptymain.h>
#include <pty-ext/ptyfs.h>
#include <pty-ext/ptysession.h>
#include <pty-ext/ptyinit.h>

#endif //LIBRARY_PTY_EXT_PTY_EXT_H

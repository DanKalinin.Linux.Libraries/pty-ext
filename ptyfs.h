//
// Created by root on 05.12.2021.
//

#ifndef LIBRARY_PTY_EXT_PTYFS_H
#define LIBRARY_PTY_EXT_PTYFS_H

#include "ptymain.h"

G_BEGIN_DECLS

gboolean pty_fs_mount_sync(gchar *source, gchar *directory, GError **error);
gboolean pty_fs_mount_v1_sync(gpointer source_data, gsize source_n, gchar *directory, GError **error);

G_END_DECLS

#endif //LIBRARY_PTY_EXT_PTYFS_H
